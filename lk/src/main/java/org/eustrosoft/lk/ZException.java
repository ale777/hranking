package org.eustrosoft.lk;

// ConcepTIS project
// (c) Alex V Eustrop 2009-2010
// see LICENSE at the project's root directory
//
// $Id$
//

/** exception which this package able to throw.
 */
public class ZException extends Exception
{

   public ZException(String s){super(s);}
   ZException(Exception e){super(e.getMessage());}

} //ZException
