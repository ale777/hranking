package org.eustrosoft.lk;

import java.util.Date;

public abstract class Record implements Comparable {

    protected String caption;
    protected Date date;

    public abstract Long getID();
    public abstract String getKey();
    public abstract String getCaption();
    public abstract Date getDate();

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public int compareTo(Object o)
    {
        Record r = (Record)o; // throws ClassCastException if non Record passed
        if(r==null) return(1);
        if(r==this) return(0);
        int cmp_status = cmp_keys(getKey(), r.getKey());
        if(cmp_status == 0) cmp_status = cmp_keys(getID(), r.getID());
        return(cmp_status);
    } // compareTo

    private int cmp_keys(Comparable my_key, Comparable its_key)
    {
        if(my_key == null) { if( its_key != null) return(1); else return(0); }
        if(its_key == null) return(-1);
        return(my_key.compareTo(its_key));
    }

    public String toString() {
        return(getCaption());
    }
}
