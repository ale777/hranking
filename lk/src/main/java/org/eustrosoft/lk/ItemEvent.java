package org.eustrosoft.lk;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ItemEvent extends Record {

    private static Long ID = 0L;

    public ItemEvent() {
        ID++;
        date = new Date();
    }

    @Override
    public Long getID() {
        return ID;
    }

    @Override
    public String getKey() {
        return null;
    }

    @Override
    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = "Комментарий к событию: " + caption;
    }

    @Override
    public Date getDate() {
        return null;
    }
}
