package org.eustrosoft.lk;

import java.util.Date;
import java.util.List;

public class Dealer extends Record {

    private List<Item> products;
    private String code;
    private String site;
    private String phoneNumber;
    private boolean active;
    private static Long ID = 0L;

    public Dealer() {
        ID++;
        date = new Date();
    }

    public List<Item> getProducts() {
        return products;
    }
    public void setProducts(List<Item> products) {
        this.products = products;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getSite() {
        return site;
    }
    public void setSite(String site) {
        this.site = site;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isActive() {
        return active;
    }
    public void setActive(boolean active) { this.active = active; }

    public void setCaption(String caption) { this.caption = "Дилер: " + caption; }

    @Override
    public Long getID() { return ID; }

    @Override
    public String getKey() { return null; }

    @Override
    public String getCaption() { return caption; }

    @Override
    public Date getDate() { return date; }
}
