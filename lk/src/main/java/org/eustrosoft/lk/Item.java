package org.eustrosoft.lk;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

public class Item extends Record {

    private static Long ID = 0L;
    private List<ItemEvent> itemEvents;
    private Hashtable<String, String> itemSpecifications;

    public Item() {
        itemEvents = new ArrayList<>(6);
        itemSpecifications = new Hashtable<>(12);
        ID++;
        date = new Date();
    }

    public List<ItemEvent> getItemEvents() {
        return itemEvents;
    }

    public boolean addEvent(ItemEvent event) {
        try { itemEvents.add(event); return true; }
        catch (Exception ex) { return false; }
    }

    public boolean removeEvent(ItemEvent event) {
        try { itemEvents.remove(event); return true;}
        catch (Exception ex) { return false; }
    }

    public boolean clearEvents() {
        try { itemEvents = new ArrayList<>(6); return true; }
        catch (Exception ex) { return false; }
    }

    public boolean addItemSpecification(String key, String value) {
        itemSpecifications.put(key, value);
        return true;
    }

    public Hashtable<String, String> getItemSpecifications() {
        return itemSpecifications;
    }

    @Override
    public Long getID() {
        return ID;
    }

    @Override
    public String getKey() {
        return null;
    }

    @Override
    public String getCaption() {
        return caption;
    }

    @Override
    public Date getDate() {
        return date;
    }
}
