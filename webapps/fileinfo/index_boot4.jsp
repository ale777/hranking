<%@ page contentType="text/html;charset=UTF-8" language="java"
         import="java.util.*"
         import="java.math.BigDecimal"
         import="java.io.*"
         import="java.text.SimpleDateFormat"
        import="java.lang.*"
        
%>

<%!

public static String text2html(String textin) {
String textout="";
String[] HTML_UNSAFE_CHARACTERS = {"<",">", "&", "\n"," "};
String[] HTML_UNSAFE_CHARACTERS_SUBST = {"&lt;", "&gt;", "&amp;", "<br>\n","&nbsp;"};
char[] textArray = textin.toCharArray();
for (char c:textArray) {
  
    String word = String.valueOf(c);
        for (int j=0;j<HTML_UNSAFE_CHARACTERS.length;j++) {
            if (HTML_UNSAFE_CHARACTERS[j].equals(word)) {
            word = HTML_UNSAFE_CHARACTERS_SUBST[j];
            break;
            }
        }
    textout = textout + word;
}
return textout;
}

%>

<%  String dir="";
    String dirpath="";
    //dir="<a href=qxyz.ru>qxyz.ru/";
    dir="/usr/share/";
    dirpath = text2html(dir);
    File actual = null;
    try {
     actual = new File(dirpath);
%>
<!Doctype HTML>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">  <!-- SIC! external-ref (см выше) -->
   <link href="css/style.css" rel="stylesheet">
   <!-- <link rel="shortcut icon" href="img/user.png" type="image/png"> -->
   <link rel="icon" href="img/user.png" type="image/png"> 
   <title>Просмотр файлов </title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col">
          <h3><%out.println("Содержание директории: "+dirpath);%> </h3>
        </div>
    <div class="col" align="right">
          <div class="dropright"> 
            <button
              class="btn btn-light btn-lg dropdown-toggle"
              type="button"
              id="dropdownMenuButton"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
             <i class="icon-folder-open">   Сервер</i>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <h5 class="dropdown-header">Обращение к серверу</h5>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Загрузить</a>
              <a class="dropdown-item" href="#">Выгрузить</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Что-то ещё</a>
            </div>
          </div>
        </div>
    </div>
  	    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">Имя</th>
            <th scope="col">Путь</th>
            <th scope="col">Свойство</th>
            <th scope="col">Последняя модификация</th>
            <th scope="col">Размер,байт</th>
        </tr>
        </thead>
<tbody>
<%
    String ico="";
    String readwrite="";
%>    
    <tr>
    <td scope="row" class="viewer"><i class="icon-share"> . . . </i></th>
    <td scope="row">____</td>
    <td scope="row">____</td>
    <td scope="row" align="center">____</td>
    <td scope="row" align="right">____</td>
    </tr>
<%  for( File f : actual.listFiles()) {
        if (f.isDirectory()&!f.isFile()) {ico="<i class=\"icon-folder\"></i>";}
        else if(!f.isDirectory()&f.isFile()){ico="<i class=\"icon-file-text2\"></i>";}
            else ico="<i class=\"icon-link\" ></i>";

    if (f.canWrite()&f.canRead()) {readwrite="чтение/запись";}
        else if (!f.canWrite()&f.canRead()){readwrite="чтение";}
            else {readwrite="запись";}
%>
<tr>
    <td scope="row" class="viewer"><%out.println (ico+"   "+f.getName());%></th>
    <td scope="row"><%=f.getPath() %></td>
    <td scope="row"><%out.println(readwrite);%></td>
    <td scope="row" align="center"><%=new SimpleDateFormat("dd.MM.yy HH:mm").format(f.lastModified())%></td>
    <td scope="row" align="right"><%=f.length()%></td>
</tr>
<%
        } //for( File f : actual.listFiles())
}catch (IOException ex){
out.println("Ошибка ввода вывода : " + ex); 
ex.printStackTrace(); 
}
catch(Exception e){ 
out.println("Нераспознанная ошибка: " + e); 
out.println("попробуйте другую операцию" ); 
}
finally{ 
}
%>
</tbody>
</table>
</div>
<script>
    let viewers=document.getElementsByClassName("viewer");

    for (let i=0;i<viewers.length;i++){
        viewers[i].addEventListener('click',()=>{
            let item = event.currentTarget.nextSibling.innerText; 
            alert(item);
        });
    }
</script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> 
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script> 
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
