let position = 250;
let position_y='';
var delimiter = '|';
var names = [];

function getXmlHttp(){
  let xmlhttp;
  try {
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e) {
    try {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
      xmlhttp = false;
    }
  }
  if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
    xmlhttp = new XMLHttpRequest();
  };
  return xmlhttp;
}

function sendName(){
    var response = '';

    let XHD=getXmlHttp();
    XHD.onreadystatechange=function(){
        if (this.readyState==4)
                {
            if (XHD.status===200){
                response = this.responseText;
                names = response.split(delimiter);

                if(response=="false")error();
                else {
                    document.getElementById("entering_name").hidden = true;
                    for(let j = 0; j < names.length; j++)
                        printRaw(names[j]);

                    setInterval(getMassive, 2000);
                }

            }
        }
    };
    XHD.open("POST","back_end.jsp", true);
    XHD.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    XHD.send('name='+document.getElementById("name").value);

    return false;
}
let XHD=getXmlHttp();

function getMassive() {
    var responseMassive = [];

    XHD.onreadystatechange=function(){
        if (this.readyState==4)
        {
            if (XHD.status===200){
                responseMassive = this.responseText.split(delimiter);
                if(names.length < responseMassive.length){
                    for(let j = names.length; j < responseMassive.length; j++){
                        printRaw(responseMassive[j]);
                    }
                }
                names = responseMassive;
                console.log(responseMassive);
            }
        }
    };
    XHD.open("GET","back_end.jsp?status=getnames", true);
    XHD.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    XHD.send();
}

function printRaw(userName) {
    let div = document.createElement('div');
    position_y = position + "px";
    div.innerHTML = `<div class="row justify-content-center " >
                    <div class="col-5 text-center align-items-center" id="content"><div class="input-group mb-2">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa  fa-user-o" aria-hidden="true"></i></span>
                    <span class="input-group-text">${userName}</span>
                </div>
                <input required autocomplete =off id="name" type="text" class="form-control" placeholder="Введите своё имя" value="">
            </div></div></div>`;
    document.body.append(div);
    position += 100;
}

function error() {
    alert("Error!");
}

