<%@ page contentType="text/html;charset=UTF-8" language="java"
import="java.util.*"
import="java.io.*"
import="java.text.*"
%>

<%!
static final String MT_DROPDOWN = "D";
static final String MT_SEPARATOR = "-";
static final String MT_HREF = "H";
static final String MT_CMD = "C";
static final String MT_CMD_D = "CD";
static final String MT_CMD_JS = "JS";

static final String Y = "Y";
static final String N = "N";

public static final int MENU_II_LEVEL = 0;
public static final int MENU_II_ID = 1;
public static final int MENU_II_ENABLED = 2;
public static final int MENU_II_TYPE = 3;
public static final int MENU_II_ACTION = 4;
public static final int MENU_II_CAPTION = 5;


private static final String MENU_ID_PREFIX = "mnu";
private static final String MENU_ID_SEP = "_";
private int MENU_LEVEL=0; //current level of menu

/*String[] MENU_PATH = new String[MAX_MENU_PATH]; // current path to menu
private String get_menu_id(String id){
MENU_PATH[MENU_LEVEL]=id; id=MENU_ID_PREFIX;
for(int i=0;i<MENU_LEVEL;i++){id=id + MENU_ID_SEP + MENU_PATH[i];}
return(id);
}
*/
String title="";
String str = "";
static Boolean SESSION = false;

static final String PAGE_AUTH = "auth";
static final String PAGE_LK = "lk";
static final String PAGE_PRODUCT = "prod";
static final String PAGE_PRODUCT_INFO  = "prod_info";

static String[] MENU = new String[]{
//lvl,	id,	enabled,type,	action,	caption[ru],custom
null,null,null,null,null,null,null, // this item ignored
//"-","main",Y,MT_HREF,"./","DOMINATOR",null,
"-","doc",Y,MT_CMD,"?content=lk","Личный кабинет",null,
"-","ls", Y,MT_CMD,null,"Контакты",null,
"-","view", Y,MT_CMD,null,"Каталог",null,
"-","mv", Y, MT_CMD,null,"Магазины",null,
"-","mv", Y, MT_CMD,null,"Справка",null,
null,null,null,null,null,null,null// this item ignored too
};

static String[] DEALERS_TABLE = new String[]{
"Название","Местоположение","Товарооборот","Дата последней закупки","Объем поставки"
};

static String[] PRODUCT_TABLE = new String[]{
"Опции","QR код","№ договора","Деньги по договору","Юр-лицо клиент","Тип продукта","Модель продукта","SN"
};

static String[] PRODUCT_CARD_TABLE = new String[]{
"Наименование","Значение","Пояснение"
};

static final String RIGHT_BUTTON="<svg width='1em' height='1em' viewBox='0 0 16 16' class='bi bi-chevron-right' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z'/></svg>";

static final String LEFT_BUTTON="<svg width='1em' height='1em' viewBox='0 0 16 16' class='bi bi-chevron-left' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z'/></svg>";

static final String[] PRODUCT_CARD = new String[]{
"QR-код","№ договора","дата договора","Деньги по договору","Юр-лицо поставщик","Юр-лицо клиент","Тип продукта","Модель продукта","SN","Дата производства","Дата ввоза (ГТД)","Дата продажи","Дата отправки клиенту","Дата начала гарантии","Дата окончания гарантии","Комментарий (для клиента)"
};
static final String[] PRODUCT_CARD_COMMENT = new String[]{
"QR код должен содержать ровно 8 символов алфавита:[0-9,A-F]; первые 5 - это диапазон, оставшиеся 3 - номер внутри диапазона в 16-ричном","для новых номеров можно использовать последние 4 символа QR-кода. Допустимо несколько карточек с одним номером договора.","Дата заключения договора","Деньги, причитающиеся поставщику, по договору за это изделие. Если изделий по договору несколько - заполняйте отдельные карточки.","Kто исполнитель по договору, если у нас более одного юр-лица или ИП.","Юр-лицо клиента, пока только название, но можете добавить ИНН, через запятую, или еще что-то. Последним укажите город. Напр.: EustroSoft, ..., Москва","Тип продукта.","Модель продукта","Серийный номер изделия. Возможно - серийные номера агрегатов через запятую. Потом разберемся","Дата производства изделия","Сейчас - номер ГТД. Изначально хотели указывать дату ввоза в Россию, или дату поступления на склад.","Дата продажи - видимо, дата поступления денег или гарантийного письма об оплате","Дата отправки клиенту/отгрузки со склада. Обычно - это-же дата начала гарантии.","Дата начала гарантии для конечного пользователя, т.е. при продажи дилером - задается им.","Дата окончания гарантии. Обычно + 1 год, но нет правил без исключений."," Этот комментарий виден клиенту! конфиденциальное пишите в поле 'Деньги'"
};

public void print_leftActionWindow(){
wln("<div id='left_menu' class='col-2'></div>");
wln("<div id='l_line'>");
wln("<button class='btn_switch'>");
wln(LEFT_BUTTON);
wln("</button>");
wln("</div>");
}

public void print_rightActionWindow(){
wln("<div id='r_line'>");
wln("<button class='btn_switch'>");
wln(RIGHT_BUTTON);
wln("</button>");
wln("</div>");
wln("<div id='right_menu' class='col-2'></div>");
}

public void print_container_begin(){
wln("<div class='container-fluid' style='height:calc(100vh - 60px - 50px);'>");
wln("<div class='row justify-content-center' style='height:100%; flex-wrap: nowrap;'>");
	}
public void container_begin(){print_container_begin();}

public void print_container_end(){wln("</div>");wln("</div>");}
public void container_end(){print_container_end();}

public void print_content_page_lk(){
print_leftActionWindow();
wln("<main id='menu' class='container-fluid py-md-3' role='main' style='height: auto;'>");
wln("<h1 class='bd-title text-center' id='content'> Спикок дилеров </h1>");

wln("<table class='table'>");
wln("<thead class='thead-light'>");
wln("<tr>");
for( String st : DEALERS_TABLE) {
		wln("<th scope='col'>");wln(st);wln("</th>");
}
wln("</tr>");
wln("</thead>");
wln("<tbody>");
for (int i=0;i<5;i++){
   wln("<tr>");
   	str = String.valueOf(i+1);
     w("<td scope='row' class='viewer'><a href='?content=prod'>Дилер №");w(str);wln("</a></td>");
     w("<td scope='row'>Город ");w(str);wln("</td>");
     w("<td scope='row'> ");w(str);wln(" 000 000 </td>");
     w("<td scope='row'> ");w(str);wln(" января</td>");
     w("<td scope='row'> ");w(str);wln("5 000 </td>");
   wln("</tr>");
}
wln("</tbody>");
wln("</table>");
wln("</main>");
print_rightActionWindow();
}
public void content_page_lk(){print_content_page_lk();}

public void print_content_page_product(){
print_leftActionWindow();
wln("<main id='menu' class='container-fluid py-md-3' role='main' style='height: auto;'>");
wln("<h1 class='bd-title text-center' id='content'> Спикок продуктов </h1>");

wln("<table class='table'>");
	wln("<thead class='thead-light'>");
	wln("<tr>");
for( String st : PRODUCT_TABLE) {
		wln("<th scope='col'>");wln(st);wln("</th>");
}
wln("</tr>");
wln("</thead>");
wln("<tbody>");
for (int i=0;i<8;i++){
   wln("<tr>");
     wln("<td scope='row' class='viewer'><a href='?content=prod_info'>значение 1</a></td>");
     wln("<td scope='row'>значение 2</td>");
     wln("<td scope='row'>значение 3</td>");
     wln("<td scope='row'>значение 4</td>");
     wln("<td scope='row'>значение 5</td>");
	 wln("<td scope='row'>значение 6</td>");
     wln("<td scope='row'>значение 7</td>");
     wln("<td scope='row'>значение 8</td>");
   wln("</tr>");
}
wln("</tbody>");
wln("</table>");
wln("</main>");
print_rightActionWindow();
}
public void content_page_product(){print_content_page_product();}

public void print_content_page_product_info(){
print_leftActionWindow();
wln("<main id='menu' class='container-fluid py-md-3' role='main' style='height: auto;'>");
wln("<h1 class='bd-title text-center' id='content'> Информация по продукту </h1>");

wln("<table class='table table-sm'>");
	wln("<thead class='thead-light'>");
	 wln("<tr>");
for( String st : PRODUCT_CARD_TABLE) {
		wln("<th scope='col'>");wln(st);wln("</th>");
}
wln("</tr>");
wln("</thead>");

wln("<tbody>");
for (int i=0;i<PRODUCT_CARD.length;i++){
   wln("<tr>");
     w("<td scope='row'>");w(PRODUCT_CARD[i]);w("</td>");
     w("<td scope='row'> ");w("<input id='param_"); str = String.valueOf(i);w(str);w("'></td>");
    w("<td scope='row'>");w(PRODUCT_CARD_COMMENT[i]);w("</td>");
   wln("</tr>");
}
wln("</tbody>");
wln("</table>");

wln("<div id='inputform' class='bd-title text-center'><h3>Форма ввода события</h3>");
wln("<form>");

  wln("<div class='form-row'>");
    wln("<div class='form-group col-md-3'>");
      wln("<label for='inputType'>Тип события</label>");
      wln("<input list='events' class='form-control custom-select' id='inputType' name='event_type'>");
   wln("<datalist id='events'>");
    wln("<option>Получение ГТД</option>");
    wln("<option>Получение на склад</option>");
    wln("<option>Отгрузка со склада</option>");
    wln("<option>Продажа потребителю</option>");
    wln("<option>Ремонт</option>");
    wln("<option>Гарантийный ремонт</option>");
   wln("</datalist>");
    wln("</div>");

    wln("<div class='form-group col-md-3'>");
      wln("<label for='inputDate'>Дата</label>");
      wln("<input pattern='[0-3][0-9]\\.[0-1][0-9]\\.20[0-9]{2}' placeholder='Формат даты: ДД.ММ.20ГГ' class='form-control' id='inputDate' name='event_date'>");
    wln("</div>");
  wln("</div>");

  wln("<div class='form-group'>");
   wln("<label for='inputComments'>Комментарий</label>");
    wln("<input type='text' class='form-control' id='inputComments'>");
  wln("</div>");

 wln("<button type='submit' class='btn btn-primary'>Сохранить</button>");
wln("</form>");

wln("</div>");
wln("<p></p>");
wln("<table class='table'>");
	wln("<thead class='thead-light'>");
	wln("<tr>");
            wln("<th scope='col'>Тип</th>");
            wln("<th scope='col'>Дата</th>");
            wln("<th scope='col'>Комментарии</th>");
            wln("<th scope='col'>Документы</th>");
        wln("</tr>");
        wln("</thead>");
wln("<tbody>");
for (int i=0;i<4;i++){
   wln("<tr>");
     wln("<td scope='row' class='viewer'>значение 1</td>");
     wln("<td scope='row'>значение 2</td>");
     wln("<td scope='row'>значение 3</td>");
     wln("<td scope='row' align='center'>значение 4</td>");
   wln("</tr>");
}
wln("</tbody>");
wln("</table>");
wln("</main>");
print_rightActionWindow();
}
public void content_page_product_info(){print_content_page_product_info();}


public void print_header(){
  wln("<header>");
  wln("<nav class='navbar navbar-expand-lg navbar-light bg-light'>");
  wln("<a class='navbar-brand' href='#'>");
  wln("<img src='62004065.pf1g232ra7.W215.jpg' width='30' height='30' class='d-inline-block align-center' alt='62004065.pf1g232ra7.W215.jpg'>  DOMINATOR</a>");
  wln("<button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarSupportedContent' aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'>");
  wln("<span class='navbar-toggler-icon'></span>");
  wln("</button>");
  wln("<div class='collapse navbar-collapse' id='navbarSupportedContent'>");
wln("<ul class='navbar-nav mr-auto'>");
printMenuPage();
wln("</ul>");

if (!SESSION) {
wln("<a class ='btn btn-outline-primary mr-sm-2' href='?content=auth'>Вход</a>");
    wln("<a class ='btn btn-outline-primary my-2 my-sm-0' href='?content=auth'>Регистрация</a>");
    }else{
    wln("<a class ='btn btn-outline-primary my-2 my-sm-0' href='#'>Выход</a>");
}

wln("</div>");
wln("</nav>");
wln("</header>");
}
public void header(){print_header();};

public void print_MenuPage()
{
 int item_size=MENU_II_CAPTION+2;
 int menu_count=MENU.length/(item_size);
 int i = 0;
 //int item_level=0;
 int level=1;
 for(i=0;i<menu_count;i++)
 {
  int item=i*item_size;
  if(MENU[item]==null)continue;
  level=MENU[item].length();
  String id=MENU[item + MENU_II_ID];
  //id=get_menu_id(id);
  String enabled=MENU[item + MENU_II_ENABLED];
  String type=MENU[item + MENU_II_TYPE];
  String action=MENU[item + MENU_II_ACTION];
  String caption=MENU[item + MENU_II_CAPTION];
printMenuItem(id,type,action,caption);continue;
}
}
public void printMenuPage() {print_MenuPage();}


public void print_MenuItem(String id,String type,String action, String caption){
wln("<li class='nav-item'>");
if(action==null)action="#";
w("        <a id='" + id + "' ");
w("class='nav-link' href='");
w(action);
w("'>");
w(caption);
wln("</a>");
wln("</li>");
}
public void printMenuItem(String id,String type,String action, String caption){print_MenuItem(id,type,action,caption);}


public void print_footer(){
Date dateNow = new Date();
SimpleDateFormat formatForDateNow = new SimpleDateFormat("MMMMMMMMM  yyyy");
wln("<footer class='fixed-bottom' style='background-color: #dfdfdf; height: 50px; font-size: 1em; font-family: sans-serif; font-style:italic'>");
w("<div class='footer-copyright text-center py-3'>© Copyright : Eustrsoft   ");
w(formatForDateNow.format(dateNow));
wln("</div>");
wln("</footer>");
}
public void footer(){print_footer();};

public void print_content_page_auth(){
	    wln("<div class='col-5 text-center' id='content' style='align-self: center;	padding-bottom: 40px;}'>");
	wln(" <h1 class='text-center'>Вход</h1>");
  	wln(" <h2 class='text-center'>в личный кабинет</h2>");

           wln("<form onsubmit='send(this);return false;'>");
                wln("<div class='form-group'>");
                  wln("<div class='input-group mb-2'>");
                        wln("<div class='input-group-prepend'>");
                        wln("<div class='input-group-text'><i class='fa fa-user' aria-hidden='true'></i></div>");
                        wln("</div>");
                        wln("<input required name='login' type='text' class='form-control' placeholder='Логин'>");
                   wln("</div>");
                  wln("</div>");
                wln("<div class='form-group'>");
                    wln("<div class='input-group mb-2'>");
                       wln("<div class='input-group-prepend'>");
                       wln("<div class='input-group-text'><i class='fa fa-key' aria-hidden='true'></i></div>");
                       wln("</div>");
                       wln("<input required name='pass' type='password' class='form-control' placeholder='Пароль'>");
                    wln("</div>");
                wln("</div>");
              wln("<input class='form-control btn btn-primary' type='submit' value='Войти'>");
           wln("</form>");
           wln("<span id='info' style='color:red;'> </span>");
        wln("</div>");
wln("</div>");
wln("</div>");
}
public void content_page_auth(){print_content_page_auth();};

private JspWriter out;
private void w(String s) {
boolean is_error = false;
        try { out.print(s); }
    catch (Exception e) { is_error = true; }
}
private void wln(String s){ w(s);w("\n");}
private void wln(){w("\n");}
void setMenuOut(JspWriter m_out) {out = m_out;}

%>

<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="shortcut icon" href="62004065.pf1g232ra7.W215.jpg" type="image/png">
    <link rel="icon" href="62004065.pf1g232ra7.W215.jpg" type="image/png">
    <title><%=title%></title>
    <style>
      *{
  /* margin:0;
 padding: 0; */
  box-sizing: border-box;
}
html{
  width: calc(100vw-15px);
  height: calc(100vh-50px);
}
body {
background-image: url(12450106_25bw.jpg);
background-position: center center;
background-repeat: no-repeat;
background-attachment: fixed;
background-size: cover;
width: 100%;
height: 100%;
line-height: 1.0;
}
.btn_switch{
  text-align: center;
  position: inherit;
  top: 35px;
  left: -15px;
  width: 30px;
  height: 30px;
  border: 2px solid #888;
  border-radius: 50%;
  background-color: #e9ecef;
}
#l_line,#r_line{
  position:relative;
  height: 100vh;
  width: 3px;
  background-color: #888;
}


    /*  #l_line #r_line:hover {
          display: none;
      } */
	.container-fluid {
        height:100%;
}
#inputform{
border: 4px solid #888;
border-radius: 8px;
margin:0;
padding:15px;
}

    .fa-check-circle-o{
        cursor:pointer;
        color:green;
        display:none;
    }
    .fa-check-circle-o:hover {
        color:lime;
    }
    .letter{
        color:blue;
    }
     a.disabled {
    pointer-events: none;
    cursor: default;
    color: #888;
    }
 tbody tr:hover {
    background: #eaf4ff;
   }
@media screen and (min-width: 576px) {
}
@media screen and (min-width: 768px){
}
@media screen and (min-width: 992px){
}
@media screen and (min-width: 1200px) {
}
@media screen and (min-width: 1600px) {
}
      </style>
  </head>
<body>

<%
setMenuOut(out);
header();

container_begin();
String content="";
try{content = request.getParameter("content");
switch(content){
    case PAGE_AUTH:
    SESSION = false;
    title="Вход в систему";
    content_page_auth();
    break;
    case PAGE_LK:
    SESSION = true;
    content_page_lk();
    title="Личный кабинет";
    break;
    case PAGE_PRODUCT:
    SESSION = true;
    content_page_product();
    title="Продукты";
    break;
    case PAGE_PRODUCT_INFO :
    SESSION = true;
    content_page_product_info();
    title="Информация о продукте";
    break;
    default:
	SESSION = true;
    content_page_auth();
}
}catch (Exception e){

            }

container_end();
footer();
%>

<script>
      const LEFT_BUTTON = "<%=LEFT_BUTTON%>";
      const RIGHT_BUTTON = "<%=RIGHT_BUTTON%>";
      let left_menu = document.getElementById("left_menu");
      let right_menu = document.getElementById("right_menu");
      //let menu = document.getElementById("menu");
      //const START_LINE_LEFT = left_menu.offsetLeft+left_menu.offsetWidth; /* box1.offsetWidth */
      //const START_LINE_RIGHT = right_menu.offsetLeft;
      //const DIM_BOX_LEFT = left_menu.offsetWidth;
      //const DIM_BOX_RIGHT = right_menu.offsetWidth;
      //let l_line = document.getElementById("l_line");
      //let r_line = document.getElementById("r_line");
      let left_close = document.getElementsByClassName("btn_switch")[0];
      let right_close = document.getElementsByClassName("btn_switch")[1];
      left_close.onclick = function () {
        if (left_menu.style.display == "none") {
          left_menu.style.display = "block";
          left_close.innerHTML = LEFT_BUTTON;
        } else {
          left_menu.style.display = "none";
          left_close.innerHTML = RIGHT_BUTTON;
        }
		};
      right_close.onclick = function () {
        if (right_menu.style.display == "none") {
          right_menu.style.display = "block";
          right_close.innerHTML = RIGHT_BUTTON;
        } else {
          right_menu.style.display = "none";
          right_close.innerHTML = LEFT_BUTTON;
        }
      };

</script>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>

