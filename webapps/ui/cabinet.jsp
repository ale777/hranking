<%@ page import="org.eustrosoft.lk.Record" %>
<%@ page import="java.util.List" %>
<%@ page import="org.eustrosoft.lk.Dealer" %>
<%@ page import="org.eustrosoft.lk.Item" %>
<%@ page import="org.eustrosoft.lk.ItemEvent" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Hashtable" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%!
    private JspWriter out;
    private List<Record> RRecords;
    private List<Dealer> dealers;
    private List<Item> items;
    private List<ItemEvent> itemEvents;

    private static final String PARAM_DEALER = "dealer";
    private static final String PARAM_ITEM = "item";
    private static final String PARAM_EVENTS = "events";

    private void initialize() {
        dealers = new ArrayList<>();
        items = new ArrayList<>();
        itemEvents = new ArrayList<>();
    }

    private void printDealers() {
        startTable();
        startTr();
        printTd("Название");
        printTd("Код");
        printTd("Телефон");
        printTd("Сайт");
        printTd("Активность");
        endTr();
        for(int i = 0; i < dealers.size(); i++) {
            startTr();
            printTd(dealers.get(i).getCaption());
            printTd(dealers.get(i).getCode());
            printTd(dealers.get(i).getPhoneNumber());
            printTd(dealers.get(i).getSite());
            printTd(dealers.get(i).isActive() ? "Yes" : "No");
            endTr();
        }
        endTable();
    }

    private void printItems() {
        startTable();
        startTr();
        printTd("Название");
        printTd("Код");
        printTd("Телефон");
        printTd("Сайт");
        printTd("Комментарий");
        printTd("Активность");
        endTr();
        for(int i = 0; i < dealers.get(0).getProducts().size(); i++) {
            startTr();
            printTd(dealers.get(i).getProducts().get(i).getItemSpecifications().get());
            printTd(dealers.get(i).getCode());
            printTd(dealers.get(i).getPhoneNumber());
            printTd(dealers.get(i).getSite());
            printTd(dealers.get(i).getComment());
            printTd(dealers.get(i).isActive() == true ? "Yes" : "No");
            endTr();
        }
        endTable();
    }

    private void printItemEvents() {
        startTable();
        startTr();
        printTd("Название");
        printTd("Код");
        printTd("Телефон");
        printTd("Сайт");
        printTd("Комментарий");
        printTd("Активность");
        endTr();
        for(int i = 0; i < dealers.size(); i++) {
            startTr();
            printTd(dealers.get(i).getTitle());
            printTd(dealers.get(i).getCode());
            printTd(dealers.get(i).getPhoneNumber());
            printTd(dealers.get(i).getSite());
            printTd(dealers.get(i).getComment());
            printTd(dealers.get(i).isActive() == true ? "Yes" : "No");
            endTr();
        }
        endTable();
    }

    private List<Item> getItemsFromDealer(Dealer dealer) { return dealer.getProducts(); }
    private List<ItemEvent> getEventsFromItem(Item item) { return item.getItemEvents(); }

    private void startTable() { wln("<table>"); }
    private void endTable() { wln("</table>"); }
    private void startTr() { wln("<tr>"); }
    private void endTr() { wln("</tr>"); }
    private void printTd(String value) { wln("<td>" + value + "</td>");}

    private void wln(String str) {
        try {
            out.print(str);
            out.print("\n");
        } catch (Exception ex) {

        }
    }

%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    this.out = out;
    initialize();
    printDealers();
%>

</body>
</html>
