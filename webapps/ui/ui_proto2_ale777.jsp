<%@ page contentType="text/html;charset=UTF-8" language="java"
import="java.util.*"
import="java.io.*"
%>
<%!
static final String LEFT = "left";
static final String RIGHT = "right";
static final String PATH_FILL="<path fill-rule='evenodd' ";
static final String CLIP=" clip-rule='evenodd'/>\\n";
static final String D_LEFT_FIRST = "d='M5.854 4.646a.5.5 0 00-.708 0l-3 3a.5.5 0 000 .708l3 3a.5.5 0 00.708-.708L3.207 8l2.647-2.646a.5.5 0 000-.708z'";
static final String D_LEFT_SECOND = "d='M10 8a.5.5 0 00-.5-.5H3a.5.5 0 000 1h6.5A.5.5 0 0010 8zm2.5 6a.5.5 0 01-.5-.5v-11a.5.5 0 011 0v11a.5.5 0 01-.5.5z'";
static final String D_RIGHT_FIRST = "d='M10.146 4.646a.5.5 0 01.708 0l3 3a.5.5 0 010 .708l-3 3a.5.5 0 01-.708-.708L12.793 8l-2.647-2.646a.5.5 0 010-.708z'";
static final String D_RIGHT_SECOND = "d='M6 8a.5.5 0 01.5-.5H13a.5.5 0 010 1H6.5A.5.5 0 016 8zm-2.5 6a.5.5 0 01-.5-.5v-11a.5.5 0 011 0v11a.5.5 0 01-.5.5z'";
static final String SVG_BEGIN_FIRST = "<svg class='bi bi-arrow-bar-";
static final String SVG_BEGIN_SECOND = "' width='.75em' height='0.75em' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>\\n";
static final String SVG_END = "</svg>";

static final String RIGHT_BUTTON=SVG_BEGIN_FIRST + RIGHT + SVG_BEGIN_SECOND + PATH_FILL + D_RIGHT_FIRST + CLIP + PATH_FILL + D_RIGHT_SECOND + CLIP + SVG_END;
static final String LEFT_BUTTON=SVG_BEGIN_FIRST + LEFT + SVG_BEGIN_SECOND + PATH_FILL + D_LEFT_FIRST + CLIP + PATH_FILL + D_LEFT_SECOND + CLIP + SVG_END;

public void print_leftActionWindow(){
wln("<nav id='left_menu' class='col-md-2'></nav>");
wln("<div id='l_line'></div>");
wln("<button class='TISCUI_close'>");
wln(LEFT_BUTTON);
wln("</button>");
}
public void print_rightActionWindow(){
w("<button class='TISCUI_close'>\n");
w(RIGHT_BUTTON);
w("</button>\n");
w("<div id='r_line'></div>\n");
w("<nav id='right_menu' class='col-md-2'></nav>\n");
}

public void print_content(){
w("<div class='row' style='padding: 15px; height: 90%; flex-wrap: nowrap;'>\n");

//public void leftActionWindow(){print_leftActionWindow();}
print_leftActionWindow();

w("<main id='menu' class='container-fluid py-md-3' role='main' style='height: auto;'>\n");
w("<h1 class='bd-title text-center' id='content'> Навигация </h1>\n");
w("<p class='bd-lead text-center'>'Рабочий стол' для навигационных компонентов.</p>\n");
w("</main>\n");

//public void rightActionWindow(){print_rightActionWindow();}
print_rightActionWindow();

w("</div>\n");
}

public void content(){print_content();}

private JspWriter out;
private void w(String s) {
boolean is_error = false;
        try { out.print(s); }
    catch (Exception e) { is_error = true; }
}
private void wln(String s){ w(s);w("\n");}
private void wln(){w("\n");}
void setMenuOut(JspWriter m_out) {out = m_out;}
%>

<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="UTF-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
      integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
      crossorigin="anonymous"
    />
    <link rel="shortcut icon" href="beacon.png" type="image/png" />
    <link rel="icon" href="beacon.png" type="image/png" />
    <title>Working window</title>
    <style>
      body {
        width: 100%;
        height: 100vh;
        color: #333;
        background: #def;
        font-size: 1em;
        font-family: "Tahoma", sans-serif;
        line-height: 135%;
        padding: 15px;
      }
      .TISCUI_close {
        position: relative;
        top: -15px;
      }
      #l_line,#r_line{
	height: 100%;   
       	width: 3px;
        background-color: red;
      }
      
      #l_line #r_line:hover {
          display: none;
      }
    </style>
  </head>
<body>

  <!--  <header>
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="#">
          <img
            src="beacon.png"^M
            width="30"^M
            height="30"^M
            class="d-inline-block align-center"^M
            alt="beacon.png"^M
          />^M
          ConceptTIS^M
        </a>^M
        <button^M
          class="navbar-toggler"^M
          type="button"^M
          data-toggle="collapse"^M
          data-target="#navbarSupportedContent"^M
          aria-controls="navbarSupportedContent"^M
          aria-expanded="false"^M
          aria-label="Toggle navigation"^M
        >^M
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent"></div>
      </nav>
    </header> -->
<%
setMenuOut(out);
content();
%>
<script>
      const LEFT_BUTTON = "<%=LEFT_BUTTON%>";
      const RIGHT_BUTTON = "<%=RIGHT_BUTTON%>";
      let left_menu = document.getElementById("left_menu");
      let right_menu = document.getElementById("right_menu");
      let menu = document.getElementById("menu");
      const START_LINE_LEFT = left_menu.offsetWidth + 53; /* box1.offsetWidth */
      const START_LINE_RIGHT = right_menu.offsetWidth + 53;
      const DIM_BOX_LEFT = left_menu.offsetWidth - 4;
      const DIM_BOX_RIGHT = right_menu.offsetWidth - 4;
      let l_line = document.getElementById("l_line");
      let r_line = document.getElementById("r_line");
      let left_close = document.getElementsByClassName("TISCUI_close")[0];
      let right_close = document.getElementsByClassName("TISCUI_close")[1];
      left_close.onclick = function () {
        if (left_menu.style.display == "none") {
          left_menu.style.display = "block";
          left_close.innerHTML = RIGHT_BUTTON;
        } else {
          left_menu.style.display = "none";
          left_close.innerHTML = LEFT_BUTTON;
        }
					      };
      right_close.onclick = function () {
        if (right_menu.style.display == "none") {
          right_menu.style.display = "block";
          right_close.innerHTML = LEFT_BUTTON;
        } else {
          right_menu.style.display = "none";
          right_close.innerHTML = RIGHT_BUTTON;
        }
      };
      l_line.addEventListener("mousedown", dragAndDrop);
      r_line.addEventListener("mousedown", dragAndDrop);
      function dragAndDrop(event) {
        let dim_change = event.currentTarget;
        dim_change.ondragstart = function () {
          return false;
        };
        document.addEventListener("mousemove", onMouseMove);
        function moveAt(pageX) {
          dim_change.style.left = pageX + "px";
          dim_change.style.top = 12 + "px";
          let delta = START_LINE - pageX;
          let width_box1 = DIM_BOX - delta;
          let width_box2 = DIM_BOX + delta;
          if (width_box1 >= 0 && width_box2 >= 0) {
            box1.style.width = width_box1 + "px";
            box2.style.width = width_box2 + "px";
          } else {
            mouseUp();
          }
        }
        function onMouseMove(event) {
          console.log(event.pageX);
          moveAt(event.pageX);
        }
        document.addEventListener("mouseup", mouseUp);
        function mouseUp() {
          document.removeEventListener("mousemove", onMouseMove);
          dim_change.onmouseup = null;
        }
      }
    </script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>

